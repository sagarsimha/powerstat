package com.powerstat.power.powerstat.PowerState;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.powerstat.power.powerstat.Send.BatteryTaskSend;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by sagar on 16/3/15.
 */
public class PowerStateReceiverOff extends BroadcastReceiver {

    public void onReceive(Context con, Intent intent){

        SharedPreferences sharedPref = con.getSharedPreferences(("PowerPrefs"), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("Status","POWER_DISCONNECTED");
        editor.commit();

        SimpleDateFormat timeformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSZ");
        String time = timeformat.format(new Date());

        JSONObject batteryobject = new JSONObject();

        try {


            batteryobject.put("Timestamp", time);
            batteryobject.put("Type", "EVENT");
            batteryobject.put("Status","POWER_DISCONNECTED");

            SharedPreferences sharedPrefb = con.getSharedPreferences(("BatteryPrefs"), Context.MODE_PRIVATE);
            batteryobject.put("Plugged", sharedPrefb.getString("Plugged", "null"));
            batteryobject.put("Level", sharedPrefb.getString("Level", "null"));
            batteryobject.put("Health", sharedPrefb.getString("Health", "null"));
            batteryobject.put("Voltage", sharedPrefb.getString("Voltage", "null"));
            batteryobject.put("Temperature", sharedPrefb.getString("Temperature", "null"));

            SharedPreferences sharedPrefimei = con.getSharedPreferences(("IMEIPrefs"), Context.MODE_PRIVATE);
            String IMEI = sharedPrefimei.getString("IMEI", "null");
            batteryobject.put("IMEI", IMEI);

            new BatteryTaskSend(con,batteryobject,time, IMEI, "EVENT").execute();

        } catch (JSONException e) {
            e.printStackTrace();
        }



    }
}
