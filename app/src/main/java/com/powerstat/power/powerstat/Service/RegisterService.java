package com.powerstat.power.powerstat.Service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;

public class RegisterService extends Service {
	
	public Context con;

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onCreate() {
		
		con = getApplicationContext();
		
		IntentFilter filter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
		registerReceiver(com.powerstat.power.powerstat.MainActivity.BatteryReceiver, filter);
		System.out.println("Battery Service created");

	}

	@Override
	public void onDestroy() {

		unregisterReceiver(com.powerstat.power.powerstat.MainActivity.BatteryReceiver);
		System.out.println("Battery Service destroyed");
        startService(new Intent(con, RegisterService.class));

	}

}