package com.powerstat.power.powerstat.BroadcastReceivers;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.SystemClock;
import android.widget.Toast;

import com.powerstat.power.powerstat.Send.BatteryTaskSend;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by root on 9/4/15.
 */
public class AlarmReceiver extends BroadcastReceiver {

    public static PendingIntent pendingIntent_repeat;
    public static AlarmManager manager_repeat;
    public static Context con;

    @Override
    public void onReceive(Context arg0, Intent arg1) {

        con = arg0;

        Intent alarmIntent = new Intent(arg0, AlarmReceiver.class);
        pendingIntent_repeat = PendingIntent.getBroadcast(arg0, 0, alarmIntent, 0);

        manager_repeat = (AlarmManager)arg0.getSystemService(Context.ALARM_SERVICE);
        manager_repeat.setExact(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime()+900000, pendingIntent_repeat);


        // Cron Send

        SimpleDateFormat timeformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSZ");
        String time = timeformat.format(new Date());

        JSONObject batteryobject = new JSONObject();

        try {

            batteryobject.put("Timestamp", time);
            batteryobject.put("Type", "CRON");

            SharedPreferences sharedPrefp = con.getSharedPreferences(("PowerPrefs"), Context.MODE_PRIVATE);
            batteryobject.put("Status",sharedPrefp.getString("Status", "null"));

            SharedPreferences sharedPrefb = con.getSharedPreferences(("BatteryPrefs"), Context.MODE_PRIVATE);
            batteryobject.put("Plugged", sharedPrefb.getString("Plugged", "null"));
            batteryobject.put("Level", sharedPrefb.getString("Level", "null"));
            batteryobject.put("Health", sharedPrefb.getString("Health", "null"));
            batteryobject.put("Voltage", sharedPrefb.getString("Voltage", "null"));
            batteryobject.put("Temperature", sharedPrefb.getString("Temperature", "null"));

            SharedPreferences sharedPrefimei = con.getSharedPreferences(("IMEIPrefs"), Context.MODE_PRIVATE);
            String IMEI = sharedPrefimei.getString("IMEI", "null");
            batteryobject.put("IMEI", IMEI);

            new BatteryTaskSend(con,batteryobject,time,IMEI,"CRON").execute();

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public static void stopAlarm(){
        if (manager_repeat != null) {
            manager_repeat.cancel(pendingIntent_repeat);
            Toast.makeText(con, "Alarm Canceled Cron", Toast.LENGTH_SHORT).show();
        }
    }



}
