package com.powerstat.power.powerstat.Send;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Environment;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.powerstat.power.powerstat.R;

import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import static android.R.attr.format;


//////////////////////Pick latest Battery data and send /////////////////
public class BatteryTaskSend extends AsyncTask<Void, Integer, Void> {

	Context con;
    JSONObject sendjson;
	String time;
	String IMEI;
	String type;

    private DatabaseReference mDatabase;


	public BatteryTaskSend(Context con, JSONObject sendjson, String time, String IMEI,String type) {
		this.con = con;
        this.sendjson = sendjson;
		this.time = time;
		this.IMEI = IMEI;
		this.type = type;
        mDatabase = FirebaseDatabase.getInstance().getReference();
	}



	@Override
	protected void onPreExecute() {

		

	}

	@Override
	protected Void doInBackground(Void... params) {


		String[] TimeStampParts = time.split(" ");
		String DateOnly = TimeStampParts[0];
		String[] DateParts = DateOnly.split("-");
		String year = DateParts[0];
		String month = DateParts[1];
		String day = DateParts[2];

		SharedPreferences sharedPrefb = con.getSharedPreferences(("LocPrefs"), Context.MODE_PRIVATE);

		// Write a message to the database
        mDatabase.child(sharedPrefb.getString("location", "null")).child(year).child(month).child(day).child(IMEI).child(type).push().setValue(sendjson.toString());

		//WriteDataToFileStorage(sendjson.toString());
			// /////////////////////////////////////////////////////////////////////////////////////////////

		return null;
	}

	@Override
	protected void onProgressUpdate(Integer... values) {

	}

	@Override
	protected void onPostExecute(Void result) {

	}

	// /////////////// Write to file //////////////////////

	public void WriteDataToFileStorage(String data) {

		PrintWriter csvWriter;
		try {
			File direct = new File(Environment.getExternalStorageDirectory()
					+ "/PowerStatFiles");

			if (!direct.exists()) {
				if (direct.mkdir())
					; // directory is created;
			}

			File file = new File(direct, "PowerData.csv");
			if (!file.exists()) {
				file = new File(direct, "PowerData.csv");
			}
			csvWriter = new PrintWriter(new FileWriter(file, true));

			csvWriter.print(data);
			csvWriter.append('\n');

			csvWriter.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// ///////////////////////// Write to File end /////////////////

}
// ///////////////////// Pick latest Battery data and send end ////////////
