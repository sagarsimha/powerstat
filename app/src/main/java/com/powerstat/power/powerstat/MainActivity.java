package com.powerstat.power.powerstat;

import android.*;
import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.FirebaseDatabase;
import com.powerstat.power.powerstat.BroadcastReceivers.AlarmReceiver;
import com.powerstat.power.powerstat.LocationMap.LocationActivity;
import com.powerstat.power.powerstat.Send.BatteryTaskSend;
import com.powerstat.power.powerstat.Service.RegisterService;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;


public class MainActivity extends RuntimePermissionsActivity {

    public static PendingIntent pendingIntent;
    private static AlarmManager manager;
    public Context context_main;
    public TextView setlocation;

    private static FirebaseDatabase fbDatabase;

    private static final int REQUEST_PERMISSIONS = 20;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context_main = getApplicationContext();

        setlocation = (TextView) findViewById(R.id.currentlatlon);

        MainActivity.super.requestAppPermissions(new
                        String[]{android.Manifest.permission.READ_PHONE_STATE, Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION}, R.string
                        .runtime_permissions_txt
                , REQUEST_PERMISSIONS);

        if(fbDatabase == null) {
            fbDatabase = FirebaseDatabase.getInstance();
            fbDatabase.setPersistenceEnabled(true);
        }


        startService(new Intent(context_main, RegisterService.class));


    }

    @Override
    public void onPermissionsGranted(final int requestCode) {
        Toast.makeText(this, "Permissions Received.", Toast.LENGTH_LONG).show();


        // IMEI can only be fetched once the permissions are provided
        // //////// Fetching IMEI  more ///////////////
        SharedPreferences sharedPrefimei = this.getSharedPreferences(("IMEIPrefs"), Context.MODE_PRIVATE);
        SharedPreferences.Editor editorimei = sharedPrefimei.edit();

        Boolean checkstatIMEI = sharedPrefimei.getBoolean("check_IMEI", false);
        if (!checkstatIMEI) {

            TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            if (telephonyManager.getDeviceId() != null) {
                editorimei.putString("IMEI", telephonyManager.getDeviceId());
                editorimei.putBoolean("check_IMEI", true);
                editorimei.commit();
            } else {
                editorimei.putBoolean("check_IMEI", false);
                editorimei.commit();
            }
        }

        // //////////////////////////////////////////////////
    }

    public void startAlarm(View view) {

        context_main.startService(new Intent(context_main, RegisterService.class));
        onstartalarm(context_main);

    }


    @SuppressLint("NewApi")
    public static void onstartalarm(Context con){



        // Retrieve a PendingIntent that will perform a broadcast
        Intent alarmIntent = new Intent(con, AlarmReceiver.class);
        pendingIntent = PendingIntent.getBroadcast(con, 0, alarmIntent, 0);
        manager = (AlarmManager) con.getSystemService(Context.ALARM_SERVICE);
        manager.setExact(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                SystemClock.elapsedRealtime(), pendingIntent);


        // Pick data from status_on_start and update it to Status
        SharedPreferences sharedPrefstart = con.getSharedPreferences(("StartPrefs"), Context.MODE_PRIVATE);
        String onstartstatus = sharedPrefstart.getString("status_on_start", "null");

        SharedPreferences sharedPref = con.getSharedPreferences(("PowerPrefs"), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("Status", onstartstatus);
        editor.commit();

        Toast.makeText(con, sharedPrefstart.getString("status_on_start", "null") , Toast.LENGTH_SHORT).show();
        Toast.makeText(con, "Alarm Set", Toast.LENGTH_SHORT).show();


        // Make a Send to signify start
        SimpleDateFormat timeformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSZ");
        String time = timeformat.format(new Date());

        JSONObject batteryobject = new JSONObject();

        try {

            batteryobject.put("Timestamp", time);
            batteryobject.put("Type", "START");
            batteryobject.put("Status",onstartstatus);

            SharedPreferences sharedPrefb = con.getSharedPreferences(("BatteryPrefs"), Context.MODE_PRIVATE);
            batteryobject.put("Plugged", sharedPrefb.getString("Plugged", "null"));
            batteryobject.put("Level", sharedPrefb.getString("Level", "null"));
            batteryobject.put("Health", sharedPrefb.getString("Health", "null"));
            batteryobject.put("Voltage", sharedPrefb.getString("Voltage", "null"));
            batteryobject.put("Temperature", sharedPrefb.getString("Temperature", "null"));

            SharedPreferences sharedPrefimei = con.getSharedPreferences(("IMEIPrefs"), Context.MODE_PRIVATE);
            String IMEI = sharedPrefimei.getString("IMEI", "null");
            batteryobject.put("IMEI", IMEI);

            new BatteryTaskSend(con,batteryobject,time,IMEI,"START").execute();

        } catch (JSONException e) {
            e.printStackTrace();
        }




    }




    public void cancelAlarm(View view) {

        if (manager != null) {
            manager.cancel(pendingIntent);
            Toast.makeText(MainActivity.this, "Alarm Canceled MainActivity", Toast.LENGTH_SHORT).show();
        }
        AlarmReceiver.stopAlarm();

    }


    public void startlocationactivity(View view) {

        Intent intent = new Intent(getApplicationContext(),LocationActivity.class);
        startActivity(intent);

    }



    // ////////////////////////////////////////////////////////////////////////////////////

    public static BroadcastReceiver BatteryReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            boolean isPresent = intent.getBooleanExtra("present", false);
            // String technology = intent.getStringExtra("technology");
            int plugged = intent.getIntExtra("plugged", -1);
            int scale = intent.getIntExtra("scale", -1);
            int health = intent.getIntExtra("health", 0);

            int rawlevel = intent.getIntExtra("level", -1);
            int voltage = intent.getIntExtra("voltage", 0);
            int temperature = intent.getIntExtra("temperature", 0);
            float level = 0;

            Bundle bundle = intent.getExtras();

            Log.i("BatteryLevel", bundle.toString());

            if (isPresent) {
                if (rawlevel >= 0 && scale > 0) {
                    level = (rawlevel * 100) / (float)scale;
                }

				/*
				 * String info = "Battery Level: " + level + "%\n"; info +=
				 * ("Technology: " + technology + "\n"); info += ("Plugged: " +
				 * getPlugTypeString(plugged) + "\n"); info += ("Health: " +
				 * getHealthString(health) + "\n"); info += ("Status: " +
				 * getStatusString(status) + "\n"); info += ("Voltage: " +
				 * voltage + "\n"); info += ("Temperature: " + temperature +
				 * "\n");
				 */

                SharedPreferences sharedPrefupdate = context.getSharedPreferences(("BatteryPrefs"), Context.MODE_PRIVATE);
                SharedPreferences.Editor appprefupdatesedit = sharedPrefupdate.edit();

                appprefupdatesedit.putString("Level", String.valueOf(level));
                appprefupdatesedit.putString("Plugged", getPlugTypeString(plugged,context));
                appprefupdatesedit.putString("Health", getHealthString(health));
                appprefupdatesedit.putString("Voltage", String.valueOf( ((float)voltage) /1000 ));
                appprefupdatesedit.putString("Temperature", String.valueOf( ((float)temperature)/10));

                appprefupdatesedit.commit();

            } else {
                System.out.println("Battery not present!!!");
            }

        }
    };

    private static String getPlugTypeString(int plugged,Context con) {
        String plugType = "";

        switch (plugged) {

            case BatteryManager.BATTERY_PLUGGED_AC:
                plugType = "AC";

                SharedPreferences sharedPrefstartac = con.getSharedPreferences(("StartPrefs"), Context.MODE_PRIVATE);
                SharedPreferences.Editor appprefonstartseditac = sharedPrefstartac.edit();
                appprefonstartseditac.putString("status_on_start", "POWER_CONNECTED");
                appprefonstartseditac.commit();

                break;

            case BatteryManager.BATTERY_PLUGGED_USB:
                plugType = "USB";

                SharedPreferences sharedPrefstartusb = con.getSharedPreferences(("StartPrefs"), Context.MODE_PRIVATE);
                SharedPreferences.Editor appprefonstartseditusb = sharedPrefstartusb.edit();
                appprefonstartseditusb.putString("status_on_start", "POWER_CONNECTED");
                appprefonstartseditusb.commit();

                break;

            default:

                plugType = "Unplugged";
                SharedPreferences sharedPrefstartd = con.getSharedPreferences(("StartPrefs"), Context.MODE_PRIVATE);
                SharedPreferences.Editor appprefonstartseditd = sharedPrefstartd.edit();
                appprefonstartseditd.putString("status_on_start", "POWER_DISCONNECTED");
                appprefonstartseditd.commit();

                break;


        }

        return plugType;
    }

    private static String getHealthString(int health) {
        String healthString = "Unknown";

        switch (health) {
            case BatteryManager.BATTERY_HEALTH_DEAD:
                healthString = "Dead";
                break;
            case BatteryManager.BATTERY_HEALTH_GOOD:
                healthString = "Good";
                break;
            case BatteryManager.BATTERY_HEALTH_OVER_VOLTAGE:
                healthString = "Over Voltage";
                break;
            case BatteryManager.BATTERY_HEALTH_OVERHEAT:
                healthString = "Over Heat";
                break;
            case BatteryManager.BATTERY_HEALTH_UNSPECIFIED_FAILURE:
                healthString = "Failure";
                break;
        }

        return healthString;
    }

    // //////////////////////////////////////////////////////////////////////////////

    @Override
    public void onResume(){
        super.onResume();
        // put your code here...
        // Display LatLong if set
        SharedPreferences sharedPrefb = this.getSharedPreferences(("LocPrefs"), Context.MODE_PRIVATE);
        String location = sharedPrefb.getString("location", "null");

        if(location != "null"){

            String[] locationParts = location.split(",");
            String lat = locationParts[0].replace("*",".");
            String lon = locationParts[1].replace("*",".");
            setlocation.setText(lat+"\n"+lon);

        }
        else{
            setlocation.setText("Not Set. Click Set Location to set");
        }


    }



}
